# CheapCode change log

## Version 1.0.0 August 10, 2021

- New: Added Index page template
- New: Added Museo Cyrl and Circe fonts
- New: Added jQuery, jQuery maskedinput
- New: Added Pug, SCSS, PostCSS
- New: Added Webpack, Babel

## Project init July 05, 2021
