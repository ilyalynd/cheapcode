'use strict'

import '../node_modules/jquery.maskedinput/src/jquery.maskedinput'

// js
import './js/main'
import './js/form'
import './js/popup'

// css
import './assets/css/main.css'

// scss
import './assets/scss/main.scss'
