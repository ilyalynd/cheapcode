(function ($) {
  'use strict'

  function openPopup(url) {
    $('#popup .overlay').fadeIn(300, function () {
      $('html').css({ 'overflow-y': 'hidden', 'overflow-x': 'hidden' })
      $('#popup, #popup .overlay').show()
      $(url).css('display', 'flex').animate({ opacity: 1 }, 300)
    })
  }

  function closePopup() {
    $('#popup .module').animate({ opacity: 0 }, 300, function () {
      history.pushState('', document.title, window.location.pathname)

      $('#popup .overlay').fadeOut(300)
      $('#popup, #popup .overlay, #popup .module').hide()
      $('html').css('overflow-y', 'visible')
    })
  }

  let url = decodeURI(window.location.hash),
      hashes = ['request']

  // открывает окно
  $('.open-popup').on('click', function (event) {
    event.preventDefault()

    let url = $(this).attr('href')

    openPopup(url)
    event.stopPropagation()
  })

  // ..при загрузке страницы
  $.each(hashes, function (index, value) {
    if (url.replace('#', '') == value.toString()) openPopup(url)
  })

  // ..при динамическом изменении
  window.addEventListener('hashchange', (event) => {
    url = decodeURI(window.location.hash)

    if (url != '') openPopup(url)

    event.stopPropagation()
  })

  // закрывает окно
  $('.close-popup').on('click', function (event) {
    event.preventDefault()
    closePopup()
  })

  // ..при нажатии на клавишу Escape
  $('body').on('keydown', function (event) {
    if (event.code == 'Escape') closePopup()
  })
}(jQuery))
