(function ($) {
  'use strict'

  // открывает вкладки
  let tab = $('ul.services-list > li')

  tab.hide().filter('#startups').show()

  $('.services-nav a').on('click', function (event) {
    event.preventDefault()

    $('.services-nav a').removeClass('active')
    tab.hide().filter(this.hash).show()
    $(this).addClass('active')
  })

  // добавляет маску к номеру телефона
  $('#phone, #request-phone').mask('+7 (999) 999-9999', { autoclear: false })

  // меняет значение чекбокса
  $('.choice a').on('click', function (event) {
    event.preventDefault()

    let parent = $(this).parents('fieldset'),
        val = $(this).attr('data-value')

    $('#request-type').val(val)

    if (!$(this).hasClass('active')) {
      parent.find('.choice a').removeClass('active')
      $(this).addClass('active')
    }
  })
}(jQuery))
